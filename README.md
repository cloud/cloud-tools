# Cloud tools

In this repository you can find usefull tools/scripts related with cloud.

## Project usage to influx

Script copies Openstack usage to influx DB

## Swap
Script creates Swap partition on VM instance.

### Usage
See help in the script menu

## Toolbox
Script runs docker container with all necessary tools required for work with Openstack.

### Icluded tools
* python 3.8
* ansible
* openstack client
* terraform

### Usage
* Copy your application credentials `rc` file to the same directory with `toolbox.sh`.
* Run `# toolbox.sh -p` and apply your appllication credentials inside container `source <app_credentials.sh>`

