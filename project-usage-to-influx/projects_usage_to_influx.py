#! /usr/bin/python3

import json
from influxdb import InfluxDBClient
import argparse


def validate_options():
    parser = argparse.ArgumentParser(description='Input parameters', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-H', '--host', dest='host', help='Hostname of Influx server', required=True)
    parser.add_argument('-p', '--port', dest='port', help='Port of Influx server', required=True)
    parser.add_argument('-u', '--username', dest='username', help='Username of Influx user', required=True)
    parser.add_argument('-P', '--password', dest='password', help='Password of Influx user', required=True)
    parser.add_argument('-d', '--database', dest='database', help='Influx database', required=True)
    parser.add_argument('-f', '--file', dest='file',  help='Path to json projects usage', required=True)
    args = parser.parse_args()
    return args


def get_usage_object_for_influx(usage_object):
    inf_json_object = {
            "measurement": "project_usage",
            "tags": {
            },
            "fields": {
            }
        }

    inf_json_object['tags']['name'] = usage_object['name']
    inf_json_object['tags']['owner'] = usage_object['tags'][0]
    inf_json_object['fields']['cpu'] = float(usage_object['CPU'])
    inf_json_object['fields']['ram'] = float(usage_object['RAM'])
    inf_json_object['fields']['disk'] = float(usage_object['disk'])
    return inf_json_object


def main():
    opts = validate_options()
    client = InfluxDBClient(host=opts.host, port=opts.port, database=opts.database, username=opts.username, password=opts.password, ssl=True, verify_ssl=True)
    with open(opts.file, 'r', encoding='utf-8-sig') as json_file:
        projects = json.load(json_file)
    inf_usage_list = []
    for project in projects:
        if project['tags']:
            inf_usage_list.append(get_usage_object_for_influx(project))
    client.write_points(inf_usage_list)


if __name__ == '__main__':
    main()
