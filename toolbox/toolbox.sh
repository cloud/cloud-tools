#!/bin/bash
# Toolbox is a script for running openstack openstack cloud compatible environment
# with tools and nice PS1 with a Whale

print_usage() {
  printf "Usage: Just run ./toolbox.sh\n"
  printf "  -p : pull image before running it\n"
}

HOST_KRB5CONF="/etc/krb5.conf"
HOST_LOCALTIME="/etc/localtime"
TOOLBOX="registry.gitlab.ics.muni.cz:443/cloud/cloud-toolbox/toolbox"
REPO_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TMP_DIR="/tmp/cloud-toolbox"

GUEST_HOME="/home/cloud"
GUEST_KRB5CONF=$HOST_KRB5CONF
GUEST_LOCALTIME=$HOST_LOCALTIME
GUEST_VOLUME="cloud-toolbox"

while getopts 'ph' option
do
case "${option}"
in
p) docker pull "$TOOLBOX" && docker image prune -f ;;
h) print_usage
   exit 1 ;;
esac
done

# On MacOS it is not possible to bind anything in /etc
# make a copy and bind that instead
if [[ "$OSTYPE" == "darwin"* ]]; then
  mkdir -p "$TMP_DIR"

  cp "$HOST_KRB5CONF" "${TMP_DIR}/krb5.conf"
  HOST_KRB5CONF="${TMP_DIR}/krb5.conf"

  cp "$HOST_LOCALTIME" "${TMP_DIR}/localtime"
  HOST_LOCALTIME="${TMP_DIR}/localtime"
fi

if [ -z "$SSH_AUTH_SOCK" ]; then
  printf "Set SSH_AUTH_SOCK with path to ssh-agent's auth socket\n"
  exit 1
fi

docker volume create --name "$GUEST_VOLUME" 1> /dev/null

docker run -it --rm \
  --net=host \
  -e KRB5CCNAME="FILE:${GUEST_HOME}/.krb5_ticket" \
  -e SSH_AUTH_SOCK=/ssh-agent \
  -e LC_ALL=C.UTF-8 \
  -v "${SSH_AUTH_SOCK}:/ssh-agent" \
  -v "${HOST_LOCALTIME}:${GUEST_LOCALTIME}:ro" \
  -v "${GUEST_VOLUME}:${GUEST_HOME}" \
  -v "${REPO_DIR}/.toolbox_bashrc:${GUEST_HOME}/.bashrc:ro" \
  -v "${REPO_DIR}:${GUEST_HOME}/repo:z" \
  -v "${HOST_KRB5CONF}:${GUEST_KRB5CONF}:ro" \
  $TOOLBOX /bin/bash
