#!/bin/sh

# System utilities: dd, mount, mkswap, swapon, swapoff, chmod, cat, grep, echo, rm, cp, mv, exit

# Global variables
SWAP_FILE="${SWAP_FILE:=/swapfile}"
SWAP_FILE_PERM=0600
SWAP_ACTION_DEFAULT='help'
SWAP_ACTION="${SWAP_ACTION:=$1}"
SWAP_SIZE_DEFAULT_MAX=64
SWAP_SIZE_DEFAULT_MIN=1
SWAP_SIZE_DEFAULT=$((SWAP_SIZE_DEFAULT_MAX / 16))
SWAP_SIZE=${SWAP_SIZE:=$2}
SWAP_BLOCK_SIZE_DEFAULT_MAX=16384
SWAP_BLOCK_SIZE_DEFAULT_MIN=512
SWAP_BLOCK_SIZE_DEFAULT=$SWAP_BLOCK_SIZE_DEFAULT_MIN
SWAP_BLOCK_SIZE=${SWAP_BLOCK_SIZE:=$3}

FSTAB_FILE='/etc/fstab'
FSTAB_BAKCUP_FILE="${FSTAB_FILE}.back"
FSTAB_SWAP_FILE="${FSTAB_FILE}.swapfile"
FSTAB_SWAP_FILE_LINE="${SWAP_FILE}       none       swap       sw       0 0"




####    HELP




# Help message
swap_file_help_message()
{
    cat << _H_E_L_P

Usage: swap.sh [action] [swap_size] [swap_block_size]

SWAP file create/remove on the system.

Actions (by default: help):
  c, create  SWAP file create. By default: ${SWAP_FILE}
  r, remove  SWAP file remove. By default: ${SWAP_FILE}
  h, help    Print this message.

Arguments:
  [swap_size]        Set size of SWAP file (gigabytes).
                       By default: ${SWAP_SIZE_DEFAULT}, min=${SWAP_SIZE_DEFAULT_MIN}, max=${SWAP_SIZE_DEFAULT_MAX}.
  [swap_block_size]  Set block size of SWAP file (bytes).
                       By default: ${SWAP_BLOCK_SIZE_DEFAULT}, min=${SWAP_BLOCK_SIZE_DEFAULT_MIN}, max=${SWAP_BLOCK_SIZE_DEFAULT_MAX}.

Environment variables:
  SWAP_FILE        Set full path of SWAP file.
  SWAP_ACTION      Set action for the script running.
  SWAP_SIZE        Set size of SWAP file.
  SWAP_BLOCK_SIZE  Set block size of SWAP file.
  SWAP_FILE        Set SWAP file path.

_H_E_L_P
}

# Print help message
swap_file_help()
{
    swap_file_help_message

    exit 0
}

# Print help, error message and exit
swap_file_error()
{
    swap_file_help_message

    echo ''
    echo "ERROR: $*"

    exit 1
}




####    DEFINITION (action, size, block size)




# SWAP size check as integer
_size_check_as_integer()
{
    if [ -z "$1" ]; then
        swap_file_error "SWAP ${2}size is empty"
    else
        echo "$1" | grep -Eq '^[0-9]+$' > /dev/null
        if [ ! $? -eq 0 ]; then
            swap_file_error "Invalid SWAP ${2}size '$1'." "Must be integer"
        fi
    fi
}

# Min size check
_size_check_as_min()
{
    if [ $2 -gt $1 ]; then
        swap_file_error "Invalid SWAP ${3}size '$1'." "Must be min=$2"
    fi
}

# Max size check
_size_check_as_max()
{
    if [ $1 -gt $2 ]; then
        swap_file_error "Invalid SWAP ${3}size '$1'." "Must be max=$2"
    fi
}

# SWAP action define/check
swap_file_action_define()
{
    case "$SWAP_ACTION" in
        create | c )
            SWAP_ACTION='create'
        ;;
        remove | r )
            SWAP_ACTION='remove'
        ;;
        help | h | '' )
            SWAP_ACTION="$SWAP_ACTION_DEFAULT"
        ;;
        * )
            swap_file_error "Invalid action '$SWAP_ACTION'"
        ;;
    esac
}

# SWAP size define/check
swap_file_size_define()
{
    case "$SWAP_SIZE" in
        '' )
            SWAP_SIZE=$SWAP_SIZE_DEFAULT
        ;;
        * )
            swap_file_size_check
        ;;
    esac
}

# SWAP size check
swap_file_size_check()
{
    _size_check_as_integer "$SWAP_SIZE" ''
    _size_check_as_min "$SWAP_SIZE" "$SWAP_SIZE_DEFAULT_MIN" ''
    _size_check_as_max "$SWAP_SIZE" "$SWAP_SIZE_DEFAULT_MAX" ''
}

# SWAP block size define/check
swap_file_block_size_define()
{
    case "$SWAP_BLOCK_SIZE" in
        '' )
            SWAP_BLOCK_SIZE=$SWAP_BLOCK_SIZE_DEFAULT
        ;;
        * )
            swap_file_block_size_check
        ;;
    esac
}

# SWAP block size check
swap_file_block_size_check()
{
    local x_block_size=$SWAP_BLOCK_SIZE_DEFAULT_MIN
    local str_block='block '

    _size_check_as_integer "$SWAP_BLOCK_SIZE" "$str_block"
    _size_check_as_min "$SWAP_BLOCK_SIZE" "$SWAP_BLOCK_SIZE_DEFAULT_MIN" "$str_block"
    _size_check_as_max "$SWAP_BLOCK_SIZE" "$SWAP_BLOCK_SIZE_DEFAULT_MAX" "$str_block"

    # Validation
    while true; do
        if [ "$x_block_size" = "$SWAP_BLOCK_SIZE" ]; then
            break
        else
            # Block size error
            if [ $x_block_size -gt $SWAP_BLOCK_SIZE ]; then
                swap_file_error "Invalid SWAP block size '$SWAP_BLOCK_SIZE'." \
                    "Must be '$x_block_size'"
            fi
        fi

        x_block_size=$(($x_block_size * 2))
    done
}




####    SWAP CREATION




# SWAP file create on system
swap_file_create_on_system()
{
    local block_size=$(($SWAP_SIZE * 1024 * 1024 * 1024 / $SWAP_BLOCK_SIZE))

    echo "SWAP file creating..."
    dd if=/dev/zero of=$SWAP_FILE bs=$SWAP_BLOCK_SIZE count=$block_size status=progress
    if [ $? -eq 0 ]; then
        chmod $SWAP_FILE_PERM "$SWAP_FILE"
        echo "File '$SWAP_FILE' has been created"
    else
        swap_file_error "File '$SWAP_FILE' has not been created"
    fi
}

# SWAP make
swap_file_make()
{
    mkswap "$SWAP_FILE"
    if [ ! $? -eq 0 ]; then
        swap_file_error "SWAP has not been maked"
    fi

    swapon "$SWAP_FILE"
    if [ ! $? -eq 0 ]; then
        swap_file_error "SWAP file has not been swiched on"
    fi
}

# SWAP file create
swap_file_create()
{
    if [ -f "$SWAP_FILE" ]; then
        swap_file_error "File '$SWAP_FILE' already exists"
    fi

    swap_file_create_on_system
    swap_file_make

    cp "$FSTAB_FILE" "$FSTAB_BAKCUP_FILE"
    if [ ! $? -eq 0 ]; then
        echo "WARNING: Backup file '$FSTAB_BAKCUP_FILE' has not been created"
    fi

    echo "$FSTAB_SWAP_FILE_LINE" >> "$FSTAB_FILE"
    if [ $? -eq 0 ]; then
        mount -a
        echo "SWAP file has been created"
    else
        swap_file_error "SWAP file has not been added in '$FSTAB_FILE'"
    fi
}




####    SWAP REMOVING




# /etc/fstab (FSTAB_FILE) restore
swap_file_restore_fstab()
{
    cp "$FSTAB_FILE" "$FSTAB_SWAP_FILE"

    if [ -f "$FSTAB_BAKCUP_FILE" ]; then
        mv "$FSTAB_BAKCUP_FILE" "$FSTAB_FILE"
    else
        grep -vwE "$SWAP_FILE" "$FSTAB_SWAP_FILE" > "$FSTAB_FILE"
        if [ ! $? -eq 0 ]; then
            swap_file_error "File '$FSTAB_FILE' has not been updated without SWAP file"
        fi
    fi

    mount -a
}

# SWAP file remove from /etc/fstab (FSTAB_FILE)
swap_file_remove_from_fstab()
{
    cat $FSTAB_FILE | grep "$SWAP_FILE" > /dev/null
    if [ $? -eq 0 ]; then
        swap_file_restore_fstab
    else
        echo "WARNING: File '$SWAP_FILE' does not exist in '$FSTAB_FILE'"
    fi
}

# SWAP file remove
swap_file_remove()
{
    echo "SWAP file removing..."
    swap_file_remove_from_fstab

    if [ ! -f "$SWAP_FILE" ]; then
        swap_file_error "File '$SWAP_FILE' does not exist"
    fi

    swapoff "$SWAP_FILE"
    if [ $? -eq 0 ]; then
        rm -f "$SWAP_FILE"
        echo "File '$SWAP_FILE' has been removed"
    else
        swap_file_error "SWAP OFF is failed"
    fi
}




####    MAIN




# Main function
swap_file()
{
    swap_file_action_define
    swap_file_block_size_define
    swap_file_size_define

    swap_file_$SWAP_ACTION
}


# Execution if defined that
if [ -f "$0" ]; then
    swap_file

    exit 0
fi
